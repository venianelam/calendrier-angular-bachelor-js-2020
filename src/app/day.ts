export interface Day {
    id: number;
    name: string;
    info: string;
}

import { Injectable } from '@angular/core';
import { Day } from './../day';
import { DAYS } from './../mock-days';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class DayService {

  constructor(private messageService: MessageService) { }

  getDays(): Observable<Day[]> {
    this.messageService.add('DayService: fetched days');
    return of(DAYS);
  }
}


import { Component, OnInit } from '@angular/core';
import { Day } from '../../day';
import { DayService } from '../../services/day.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  days: Day[] = [];

  constructor(private dayService: DayService) { }

  ngOnInit(): void {
    this.getDays();
  }

  selectedDay: Day;
  onSelect(day: Day): void {
    this.selectedDay = day;
  }

  getDays(): void {
    this.dayService.getDays().subscribe(days => this.days = days.slice(0, 28));
  }
}

import { Component, OnInit } from '@angular/core';
import { Day } from '../../day';
import { DayService } from '../../services/day.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  constructor(private dayService: DayService) { }

  ngOnInit() {
    this.getDays();
  }

  /*day: Day = {
    id: 1,
    name: 'Lundi'
  };*/

  days: Day[];

  selectedDay: Day;
  onSelect(day: Day): void {
    this.selectedDay = day;
  }

  getDays(): void {
    this.dayService.getDays().subscribe(days => this.days = days);
  }
}

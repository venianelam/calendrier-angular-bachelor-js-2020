import { Component, OnInit, Input } from '@angular/core';
import { Day } from '../../day';
import { DayService } from '../../services/day.service';

@Component({
  selector: 'app-day-detail',
  templateUrl: './day-detail.component.html',
  styleUrls: ['./day-detail.component.scss']
})
export class DayDetailComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() day: Day;

}

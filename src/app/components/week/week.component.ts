import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Day } from '../../day';
import { DayService } from '../../services/day.service';

@Component({
  selector: 'app-week',
  templateUrl: './week.component.html',
  styleUrls: ['./week.component.scss']
})
export class WeekComponent implements OnInit {

  constructor(private route: ActivatedRoute, private dayService: DayService, private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.route.params.subscribe(params => this.id = params.id);
    //console.log(params) => checking if the id is well got
  }

  ngOnInit(): void {

    this.getWeek(this.id);
  }

  @Input() public id: number;

  days: Day[] = [];

  selectedDay: Day;
  onSelect(day: Day): void {
    this.selectedDay = day;
  }

  getWeek(id): void {
    if (id == 1) {
      this.dayService.getDays().subscribe(days => this.days = days.slice(0, 7));
    } else {
      this.dayService.getDays().subscribe(days => this.days = days.slice(7 * (id - 1), id * 7));
    }
  }
}

import { Day } from './day';

export const DAYS: Day[] = [
    // First week
    { id: 1, name: 'Jour 1', info: '' },
    { id: 2, name: 'Jour 2', info: '' },
    { id: 3, name: 'Jour 3', info: '' },
    { id: 4, name: 'Jour 4', info: '' },
    { id: 5, name: 'Jour 5', info: '' },
    { id: 6, name: 'Jour 6', info: '' },
    { id: 7, name: 'Jour 7', info: '' },
    // Second week
    { id: 8, name: 'Jour 8', info: '' },
    { id: 9, name: 'Jour 9', info: '' },
    { id: 10, name: 'Jour 10', info: '' },
    { id: 11, name: 'Jour 11', info: '' },
    { id: 12, name: 'Jour 12', info: '' },
    { id: 13, name: 'Jour 13', info: '' },
    { id: 14, name: 'Jour 14', info: '' },
    // Third week
    { id: 15, name: 'Jour 15', info: '' },
    { id: 16, name: 'Jour 16', info: '' },
    { id: 17, name: 'Jour 17', info: '' },
    { id: 18, name: 'Jour 18', info: '' },
    { id: 19, name: 'Jour 19', info: '' },
    { id: 20, name: 'Jour 20', info: '' },
    { id: 21, name: 'Jour 21', info: '' },
    // Fourth week
    { id: 22, name: 'Jour 22', info: '' },
    { id: 23, name: 'Jour 23', info: '' },
    { id: 24, name: 'Jour 24', info: '' },
    { id: 25, name: 'Jour 25', info: '' },
    { id: 26, name: 'Jour 26', info: '' },
    { id: 27, name: 'Jour 27', info: '' },
    { id: 28, name: 'Jour 28', info: '' },
];
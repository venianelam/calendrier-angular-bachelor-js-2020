import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { DayDetailComponent } from './components/day-detail/day-detail.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { WeekComponent } from './components/week/week.component';

@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    DayDetailComponent,
    DashboardComponent,
    WeekComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

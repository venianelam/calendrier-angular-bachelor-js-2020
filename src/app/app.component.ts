import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Mon calendrier personnalisé';

  description = "Désormais créez un calendrier rapidement ! Cliquez simplement sur le jour de votre choix et tapez sur la fenêtre de détails qui s'affiche ce que vous voulez (repas, événements, réunions, etc). La modification est rapide, en temps réel donc pas de prise de tête. Vous ne pouvez l'enregistrer qu'en prenant une capture d'écran ; néanmoins, il vous permettra d'avoir une première visualisation sur comment organiser votre emploi du temps. Utile pour gérer vos réunions, vos événements, ou encore les plats de la semaine !";
}

# LAM-V Calendrier Angular - Bachelor JS

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.8. 

Désormais créez un calendrier rapidement ! Cliquez simplement sur le jour de votre choix et tapez sur la fenêtre de détails qui s'affiche ce que vous voulez (repas, événements, réunions, etc). La modification est rapide, en temps réel donc pas de prise de tête. Vous ne pouvez l'enregistrer qu'en prenant une capture d'écran ; néanmoins, il vous permettra d'avoir une première visualisation sur comment organiser votre emploi du temps. Utile pour gérer vos réunions, vos événements, ou encore les plats de la semaine !

## Pour lancer le projet - FR

* Lancez votre IDE avec le projet 

* Ouvrez un terminal dans votre IDE

* Pour éviter tout problème de lancement, faîtes un npm install

* Une fois fini, faîtes un ng serve

* Dans votre navigateur (Google Chrome par exemple), entrez dans votre barre d'URL localhost:4200

## Plus d'informations

Vous trouverez la documentation dans le fichier 
* Projet JS - Bachelor 2020 - LAM V.pdf

# Below this line, is the angular tutorial automatically generated 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
